/**
 * Author: Ramin Bakhshpour (ramin.bp @ gmail.com)
 */

process.env.NODE_ENV = 'test';


const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');
const should = chai.should();
const appUrlPrefix = '/api/v1';

const {Inventory} = require('../app/models');

chai.use(chaiHttp);


describe('Inventory', () => {

    beforeEach(done => {
        Inventory.destroy({where: {}}).then(() => done());
    });

    describe('GET /inventory', () => {
        it('should GET all the inventory items', (done) => {

            chai.request(server)
                .get(appUrlPrefix + '/inventory')
                .end((error, result) => {
                    result.should.have.status(200);
                    result.body.should.be.a('array');
                    result.body.length.should.be.eq(0);
                    done();
                });
        });
    })
});