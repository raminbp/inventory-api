/**
 * Author: Ramin Bakhshpour (ramin.bp @ gmail.com)
 */
/**
 * Author: Ramin Bakhshpour (ramin.bp @ gmail.com)
 */

process.env.NODE_ENV = 'test';


const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');
const should = chai.should();
const appUrlPrefix = '/api/v1';

const {Inventory, Purchase} = require('../app/models');

chai.use(chaiHttp);


describe('Purchase', () => {

    beforeEach(done => {
        Inventory.destroy({where: {}}).then(
            Purchase.destroy({where: {}}).then(() => done())
        );

    });

    describe('POST /inventory/buy', () => {
        it('should create new items in inventory and return created purchase item', (done) => {

            const purchase = {
                purchaseDate: new Date(),
                price: 100,
                quantity: 20
            };

            chai.request(server)
                .post(appUrlPrefix + '/inventory/buy')
                .send(purchase)
                .end((error, result) => {
                    result.should.have.status(200);
                    result.body.should.be.a('object');
                    result.body.id.should.not.be.eq(null);


                    chai.request(server)
                        .get(appUrlPrefix + '/inventory')
                        .end((error, result) => {
                            result.should.have.status(200);
                            result.body.should.be.a('array');
                            result.body.length.should.be.eq(1);
                            done();
                        });

                });


        });
    })
});