/**
 * Author: Ramin Bakhshpour (ramin.bp @ gmail.com)
 */
const appRoot = require('app-root-path');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf } = format;




const customFormat = printf(info => {
    return `${info.timestamp} ${info.level}: ${info.message}`;
});

const options = {
    file: {
        level: 'info',
        filename: `${appRoot}/app.log`,
        handleExceptions: true,
        format: combine(
            timestamp(),
            customFormat
        ),
        json: true,
        maxsize: 5242880, //5 mb
        maxFiles: 10,
        colorize: false
    },
    console: {
        level: 'debug',
        format: format.simple(),
        handleExceptions: true,
        json: false,
        colorize: true
    }
};

const logger = createLogger({
    transports: [
        new transports.File(options.file),
        new transports.Console(options.console)
    ],
    exitOnError: false
});

logger.stream = {
    write: function(message, encoding) {
        logger.info(message);
    },
};

module.exports = logger;