module.exports = {
    up: (queryInterface, Sequelize) =>
        queryInterface.createTable('Inventory', {
            id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true
            },
            purchaseId: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'Purchase',
                    key: 'id',
                    as: 'purchaseId'
                }
            },
            quantity: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            }
        }),
    down: (queryInterface, Sequelize) => queryInterface.dropTable('Inventory')
};