module.exports = {
    up: (queryInterface, Sequelize) =>
        queryInterface.createTable('Sell', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            sellDate: {
                type: Sequelize.DATE,
                allowNull: false,
            },
            quantity: {
                type: Sequelize.DATE,
                allowNull: false
            },
            price: {
                type: Sequelize.DATE,
                allowNull: false
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        }),
    down: (queryInterface) => queryInterface.dropTable('Sell'),
};