/**
 * Author: Ramin Bakhshpour (ramin.bp @ gmail.com)
 */


const inventoryRoute = require('./inventory');

module.exports = (router) => {
    router.route('/inventory')
        .get(inventoryRoute.getInventory);

    router.route('/inventory/buy')
        .post(inventoryRoute.buyItem);

    router.route('/inventory/sell')
        .post(inventoryRoute.sellItem);

};