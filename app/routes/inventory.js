/**
 * Author: Ramin Bakhshpour (ramin.bp @ gmail.com)
 */

const inventoryController = require('../controller').inventory;


module.exports = {
    getInventory(request, response) {
        inventoryController.get()
            .then(items => response.status(200).json(items))
            .catch(error => response.status(500).send("Error happened in fetching inventory items: " + error));
    },

    buyItem(request, response) {
        inventoryController.buyItem(request.body)
            .then(item => response.status(200).json(item))
            .catch(error => response.status(500).send('Error happened in saving purchase item: ' + error));
    },

    sellItem(request, response) {
        inventoryController.sellItem(request.body)
            .then(item => response.status(200).json(item))
            .catch(error => response.status(500).send('Error happened in selling item: ' + error));
    }
};