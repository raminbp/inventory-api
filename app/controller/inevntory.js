/**
 * Author: Ramin Bakhshpour (ramin.bp @ gmail.com)
 */

const db = require('../models');
const {Purchase, Inventory, Sell} = db;
const logger = require('../../logger');


const createInventoryItemFromPurchase = (purchase) => {
    return {
        quantity: purchase.quantity
    }
};


module.exports = {

    get() {
        return new Promise((resolve, reject) => {
            logger.info("Fetching inventory items");
            Inventory.findAll()
                .then(inventory => resolve(inventory))
                .catch(error => {
                    logger.error('Error in fetching Inventory item: ' + error);
                    reject(error);
                });
        });
    },


    buyItem(item) {

        return new Promise((resolve, reject) => {

            let result = null;
            db.sequelize.transaction(t => {

                return Purchase.create(item, {transaction: t})
                    .then(item => {
                        const inventoryItem = createInventoryItemFromPurchase(item);
                        Object.assign(inventoryItem, {purchaseId: item.id});
                        return Inventory.create(inventoryItem, {transaction: t})
                            .then(item => {result = item;})
                            .catch(error => { throw error; });
                    }).catch(error=> {throw error;})

            })
            .then(() => resolve(result)).catch(error => reject(error))

        });
    },

    sellItem(item) {
        return new Promise((resolve, reject) => {
        });
    }

};