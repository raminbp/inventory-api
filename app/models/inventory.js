/**
 * Author: Ramin Bakhshpour (ramin.bp @ gmail.com)
 */

const Purchase = require('./index').Purchase;

module.exports = (sequelize, DataTypes) => {
    const Inventory = sequelize.define('Inventory', {
            quantity: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            purchaseId: {
                type: DataTypes.INTEGER,
                references: {
                    model: Purchase,
                    key: 'id',
                    as: 'purchase'
                }
            }

        },
        {
            freezeTableName: true
        });


    // Inventory.associate = (models) => {
    //     Inventory.hasOne(models.Purchase, {
    //         foreignKey: 'purchaseId',
    //         as: 'purchase'
    //     });
    // };

    return Inventory;
};