/**
 * Author: Ramin Bakhshpour (ramin.bp @ gmail.com)
 */
/**
 * Author: Ramin Bakhshpour (ramin.bp @ gmail.com)
 */
module.exports = (sequelize, DataTypes) => {
    const Purchase = sequelize.define('Purchase', {
            purchaseDate: {
                type: DataTypes.DATE,
                allowNull: false
            },
            quantity: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            price: {
                type: DataTypes.FLOAT,
                allowNull: false
            }
        },
        {
            freezeTableName: true
        });

    Purchase.associate = (models) => {
        Purchase.belongsTo(models.Inventory);
    };

    return Purchase;
};