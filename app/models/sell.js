/**
 * Author: Ramin Bakhshpour (ramin.bp @ gmail.com)
 */
/**
 * Author: Ramin Bakhshpour (ramin.bp @ gmail.com)
 */
module.exports = (sequelize, DataTypes) => {
    const Sell = sequelize.define('Sell', {
            sellDate: {
                type: DataTypes.DATE,
                allowNull: false
            },
            quantity: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            price: {
                type: DataTypes.FLOAT,
                allowNull: false
            }
        },
        {
            freezeTableName: true,
        });

    return Sell;
};