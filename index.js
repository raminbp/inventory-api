const express = require('express');
const bodyParser = require("body-parser");
const router = express.Router();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const logger = require('./logger');
const morgan = require('morgan');

const app = new express();

app.use(morgan("combined", { "stream": logger.stream }));
app.use(bodyParser.json());

require('./app/routes')(router);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api/v1', router);

app.listen(3000);

module.exports = app;
